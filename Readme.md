# evpf - postfix evaluator

evpf (**ev**aluate **p**ost**f**ix is a simple postfix expression evaluator
written as a learning project in rust. It supports both command line and 
interactive mode.

## Command line mode

    evpf <expression>

where `<expression>` is a postfix expression like `20 30 + 11 -` etc. 
Currently only 4 operators are supported: `+`, `-`, `*` and `/`. (In command
line mode, `*` has to be escaped using `\*` to work properly.)

## Interactive mode

Interactive mode displays a prompt where expressions can be evaluated by 
typing into the prompt (line editing and command history support using 
`rustyline`). 

    evpf> 

In interactive mode, apart from entering expressions, the following commands 
are supported

* `q` or `Q` - quit
* `?` or `h` or `H` - help

